import 'dart:async';
import 'package:besthomecare/Screens/auth_screen.dart';
import 'package:besthomecare/Screens/homeScreen.dart';
import 'package:besthomecare/utils/LinkingScreens.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

///huge problem
class VerifyScreen extends StatefulWidget {
  bool update;

  VerifyScreen(this.update);

  @override
  _VerifyScreenState createState() => _VerifyScreenState();
}

class _VerifyScreenState extends State<VerifyScreen> {
  @override
  final auth = FirebaseAuth.instance;
  User user;
  Timer timer;

  @override
  void initState() {
    user = auth.currentUser;
    user.sendEmailVerification();
    timer = Timer.periodic(Duration(seconds: 5), (timer) {
      checkEmailVerified();
    });
    super.initState();
  }

  @mustCallSuper
  @protected
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              child: Text(
                'WELCOME TO',
                style: GoogleFonts.cormorantInfant(
                    fontSize: 20, fontWeight: FontWeight.w500),
              ),
            ),
            Container(
              alignment: Alignment.topCenter,
              child: Image.asset(
                'images/backchat.png',
                height: 120,
                color: Theme.of(context).primaryColor,
              ),
            ),
            Container(
              child: Text(
                (!user.emailVerified)
                    ? 'An email has been sent to \n${user.email} \nplease verify'
                    : 'Verified',
                textAlign: TextAlign.center,
                style: GoogleFonts.cormorantInfant(
                    fontSize: 25,
                    fontWeight: FontWeight.w500,
                    color: (!user.emailVerified) ? Colors.black : Colors.green),
              ),
            ),
            InkWell(
              onTap: () {
                if (!user.emailVerified && widget.update == false) {
                  user.delete();
                  goToNextScreenAndRemove(context, AuthScreen());
                }
                // if (!user.emailVerified && widget.update == true) {
                //   user.updateEmail(ProfilePage.oldEmail);
                //   goToNextScreenAndRemove(context, chatPage());
                // }
                if (user.emailVerified) {
                  goToNextScreenAndRemove(context, HomeScreen());
                }
              },
              child: Container(
                width: 206.0,
                height: 64.0,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [
                        Theme.of(context).primaryColor,
                        Colors.orangeAccent
                      ],
                    )),
                child: Text(
                  (!user.emailVerified) ? 'GO BACK' : 'GO',
                  style: GoogleFonts.cormorantInfant(
                      fontSize: 25,
                      fontWeight: FontWeight.w500,
                      color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> checkEmailVerified() async {
    user = auth.currentUser;
    await user.reload();
    setState(() {});
    if (user.emailVerified) {
      timer.cancel();
    }
  }
}
