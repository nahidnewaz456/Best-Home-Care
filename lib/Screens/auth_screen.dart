import 'package:besthomecare/Screens/homeScreen.dart';
import 'auth_form.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:besthomecare/utils/LinkingScreens.dart';
import 'package:besthomecare/utils/verifyEmail.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
  final _auth = FirebaseAuth.instance;

// Future<UserModel> getUser() async {
//   var firebaseUser = _auth.currentUser;
//
//   return UserModel(
//     firebaseUser.uid,
//     displayName: firebaseUser.email,
//   );
// }
}

class _AuthScreenState extends State<AuthScreen> {
  var _isLoading = false;

  void _submitAuthForm(
    String email,
    String password,
    String username,
    bool isLogin,
    BuildContext ctx,
  ) async {
    UserCredential authResult;

    try {
      setState(() {
        _isLoading = true;
      });
      if (isLogin) {
        authResult = await widget._auth
            .signInWithEmailAndPassword(email: email, password: password);
        print('authResult: ${authResult}');

        ///Login successfully
        setState(() {
          _isLoading = false;
        });
        goToNextScreenAndRemove(context, HomeScreen());
      } else {
        authResult = await widget._auth
            .createUserWithEmailAndPassword(email: email, password: password);

        // await FirebaseFirestore.instance
        //     .collection('users')
        //     .doc(authResult.user.uid)
        //     .set({
        //   'username': username,
        //   'email': email,
        // });
        print('authResult else: ${authResult}');

        ///Sign up successfully
        var firebaseUser = widget._auth.currentUser;

        firebaseUser.updateProfile(
          displayName: username,
        );

        setState(() {
          _isLoading = false;
        });
        goToNextScreenAndRemove(
            context,
            StreamBuilder(
                stream: FirebaseAuth.instance.authStateChanges(),
                builder: (ctx, userSnapshot) {
                  print('userSnapsht: ${userSnapshot.data}');
                  if (userSnapshot.hasData) {
                    return VerifyScreen(false);
                  }
                  return Container(
                    color: Colors.white,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                }));
      }
    } on PlatformException catch (err) {
      var message = 'An error occured please check your credentials!';
      if (err.message != null) {
        message = err.message;
      }
      ScaffoldMessenger.of(ctx).showSnackBar(SnackBar(
        content: Text('$err'),
        backgroundColor: Theme.of(ctx).errorColor,
        duration: Duration(seconds: 4),
      ));
      setState(() {
        _isLoading = false;
      });
    } catch (err) {
      print(err);
      ScaffoldMessenger.of(ctx).showSnackBar(SnackBar(
        content: Text('$err'),
        backgroundColor: Theme.of(ctx).errorColor,
        duration: Duration(seconds: 4),
      ));
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: AuthForm(
        _submitAuthForm,
        _isLoading,
      ),
    );
  }
}
