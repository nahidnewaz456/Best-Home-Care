import 'package:besthomecare/Screens/homeScreen.dart';
import 'package:besthomecare/utils/animationScaleWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login_facebook/flutter_login_facebook.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:besthomecare/utils/LinkingScreens.dart';
import 'ForgotPassword.dart';

///signin by email
class AuthForm extends StatefulWidget {
  AuthForm(this.submitFn, this.isLoading);

  final bool isLoading;
  final void Function(
    String email,
    String password,
    String userName,
    bool isLogin,
    BuildContext ctx,
  ) submitFn;

  @override
  _AuthFormState createState() => _AuthFormState();
}

class _AuthFormState extends State<AuthForm> {
  var _auth = FirebaseAuth.instance;

  var _googleSign = GoogleSignIn();
  final _formkey = GlobalKey<FormState>();
  var _isLogin = true;
  var _userEmail = "";
  var _userName = "";
  var _userPassword = "";

  void _trySubmit() {
    final _isvalid = _formkey.currentState.validate();
    FocusScope.of(context).unfocus();

    if (_isvalid) {
      _formkey.currentState.save();
      widget.submitFn(
        _userEmail.trim(),
        _userPassword.trim(),
        _userName.trim(),
        _isLogin,
        context,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    ///background
    return Stack(
      children: [
        Container(
          alignment: Alignment.topRight,
          child: Image.asset(
            'images/Polygon 2.png',
            height: 155,
          ),
        ),
        Container(
          alignment: Alignment.topRight,
          margin: EdgeInsets.only(right: 100),
          child: Image.asset(
            'images/Polygon 3.png',
            height: 73,
          ),
        ),
        Container(
          alignment: Alignment.topRight,
          margin: EdgeInsets.only(right: 140, top: 45),
          child: Image.asset(
            'images/Polygon 4.png',
            height: 155,
          ),
        ),
        Container(
          alignment: Alignment.topLeft,
          child: Image.asset(
            'images/Polygon 1.png',
            width: 132,
          ),
        ),
        Container(
          alignment: Alignment.bottomRight,
          child: Image.asset(
            'images/Polygon 5.png',
            width: 188,
            height: 186,
          ),
        ),
        AnimationScaleWidget(
          position: 1,
          child: Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(top: 20, left: 10),
            child: Image.asset(
              'images/Polygon 6.png',
              width: 218,
              height: 206,
            ),
          ),
          // horizontalOffset: 100,
          milliseconds: 1000,
        ),

        Container(
          margin: EdgeInsets.only(top: 185, left: 30),
          child: Text(
            'All your health care needs on\nyour finger tips',
            style:
                GoogleFonts.zillaSlab(fontSize: 16, color: Color(0xFFA9A9A9)),
          ),
        ),

        ///auth form
        Center(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Container(
              alignment: Alignment.topCenter,
              margin: EdgeInsets.only(top: 225, right: 20, left: 20),
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(25),
                  child: Form(
                    key: _formkey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        AnimationScaleWidget(
                          position: 2,
                          child: Container(
                            child: Text(
                              (_isLogin)
                                  ? 'Sign In to continue '
                                  : 'Create Your Profile',
                              style: GoogleFonts.cormorantInfant(
                                  fontSize: 30,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black),
                            ),
                            margin: EdgeInsets.only(bottom: 30),
                          ),
                          milliseconds: 1000,
                        ),
                        AnimationScaleWidget(
                          position: 3,
                          milliseconds: 1000,
                          child: Container(
                            margin: EdgeInsets.only(bottom: 10, top: 10),
                            height: 60,
                            child: Center(
                              child: Theme(
                                data: ThemeData(
                                  primaryColor: Color(0xFFB2A9EC),
                                ),
                                child: TextFormField(
                                  style: GoogleFonts.cormorantInfant(
                                      fontWeight: FontWeight.bold),
                                  cursorColor: Color(0xFFB2A9EC),
                                  key: ValueKey('email'),
                                  validator: (value) {
                                    if (value.isEmpty ||
                                        !value.contains('@') ||
                                        !value.contains('.')) {
                                      return 'Please enter a valid email address';
                                    }
                                    return null;
                                  },
                                  onChanged: (s) {},
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                      labelText: 'Email',
                                      labelStyle: GoogleFonts.zillaSlab(),
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xFFB2A9EC),
                                              width: 2.0),
                                          borderRadius:
                                              BorderRadius.circular(30.0)),
                                      contentPadding:
                                          EdgeInsets.only(left: 20, right: 20)),
                                  onSaved: (value) {
                                    _userEmail = value;
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                        if (!_isLogin)
                          AnimationScaleWidget(
                            position: 4,
                            milliseconds: 1000,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                              ),
                              height: 60,
                              child: Center(
                                child: Theme(
                                  data: new ThemeData(
                                    primaryColor: Color(0xFFB2A9EC),
                                  ),
                                  child: TextFormField(
                                    style: GoogleFonts.cormorantInfant(
                                        fontWeight: FontWeight.bold),
                                    key: ValueKey('username'),
                                    validator: (value) {
                                      if (value.isEmpty || value.length < 4) {
                                        return 'Username must be at leat 4 characters';
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                        labelText: 'Username',
                                        labelStyle: GoogleFonts.zillaSlab(),
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Color(0xFFB2A9EC),
                                                width: 2.0),
                                            borderRadius:
                                                BorderRadius.circular(30.0)),
                                        contentPadding: EdgeInsets.only(
                                            left: 20, right: 20)),
                                    onSaved: (value) {
                                      _userName = value;
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ),
                        AnimationScaleWidget(
                          position: 5,
                          milliseconds: 1000,
                          child: Container(
                            height: 60,
                            child: Center(
                              child: Theme(
                                data: new ThemeData(
                                  primaryColor: Color(0xFFB2A9EC),
                                ),
                                child: TextFormField(
                                  style: GoogleFonts.cormorantInfant(
                                      fontWeight: FontWeight.bold),
                                  key: ValueKey('password'),
                                  validator: (value) {
                                    if (value.isEmpty || value.length < 7) {
                                      return 'Password must be at least 7 characters long';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.only(left: 20, right: 20),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xFFB2A9EC),
                                            width: 2.0),
                                        borderRadius:
                                            BorderRadius.circular(30.0)),
                                    labelText: 'Password',
                                    labelStyle: GoogleFonts.zillaSlab(),
                                  ),
                                  onSaved: (value) {
                                    _userPassword = value;
                                  },
                                  obscureText: true,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.topRight,
                          margin: EdgeInsets.only(bottom: 10),
                          child: TextButton(
                            onPressed: () {
                              goToNextScreen(context, ForgotPassword());
                            },
                            child: Text(
                              'Forgot Password?',
                              style: GoogleFonts.cormorantInfant(
                                  fontSize: 15, color: Color(0xFFE55B86)),
                            ),
                          ),
                        ),
                        if (widget.isLoading) CircularProgressIndicator(),
                        if (!widget.isLoading)

                          ///sending data to firebase
                          AnimationScaleWidget(
                            position: 6,
                            milliseconds: 1000,
                            child: InkWell(
                              onTap: _trySubmit,
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 60.0,
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(left: 3, right: 3),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Color(0xFFB2A9EC)),
                                child: Text(
                                  _isLogin ? 'Login' : 'Signup',
                                  style: GoogleFonts.cormorantInfant(
                                      color: Colors.white,
                                      fontSize: 28,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            ),
                          ),
                        if (!widget.isLoading)
                          Row(
                            children: [
                              Text(
                                _isLogin
                                    ? '    Don’t have an account?'
                                    : '                       ',
                                style: GoogleFonts.cormorantInfant(
                                    fontSize: 15, color: Colors.black),
                              ),
                              TextButton(
                                  onPressed: () {
                                    setState(() {
                                      _isLogin = !_isLogin;
                                    });
                                  },
                                  child: Text(
                                    _isLogin
                                        ? 'Create new one'
                                        : 'I already have an account',
                                    style: GoogleFonts.cormorantInfant(
                                        fontSize: 15, color: Color(0xFF5A528B)),
                                  )),
                            ],
                          ),

                        ///social login
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Text(
                            'OR',
                            style: GoogleFonts.cormorantInfant(
                                fontSize: 25, fontWeight: FontWeight.bold),
                          ),
                        ),
                        AnimationScaleWidget(
                          position: 7,
                          milliseconds: 1000,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: facebookSignin,
                                child: Container(
                                  child: Center(
                                      child: Image.asset(
                                    'images/facebook.png',
                                    height: 25,
                                  )),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(500),
                                    color: Color(0xFF5A528B),
                                  ),
                                  height: 60,
                                  width: 60,
                                  margin: EdgeInsets.only(left: 10, top: 10),
                                ),
                              ),
                              GestureDetector(
                                onTap: google,
                                child: Container(
                                  child: Center(
                                      child: Image.asset(
                                    'images/google.png',
                                    height: 25,
                                  )),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(500),
                                    color: Color(0xFFB2A9EC),
                                  ),
                                  height: 60,
                                  width: 60,
                                  margin: EdgeInsets.only(left: 20, top: 10),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                physics: BouncingScrollPhysics(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  ///google login
  Future<User> google() async {
    GoogleSignInAccount googleSignInAccount = await _googleSign.signIn();
    GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;
    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
        idToken: googleSignInAuthentication.idToken,
        accessToken: googleSignInAuthentication.accessToken);

    final UserCredential authResult =
        await _auth.signInWithCredential(credential);
    final User user = authResult.user;

    if (user.email != null) {
      goToNextScreenAndRemove(context, HomeScreen());
    }

    return user;
  }

  ///facebook login
  facebookSignin() async {
    final fb = FacebookLogin();

// Log in
    final res = await fb.logIn(permissions: [
      FacebookPermission.publicProfile,
      FacebookPermission.email,
    ]);

// Check result status
    switch (res.status) {
      case FacebookLoginStatus.success:
        // Logged in

        // Send access token to server for validation and auth
        final FacebookAccessToken accessToken = res.accessToken;
        final AuthCredential authCredential =
            FacebookAuthProvider.credential(accessToken.token);
        final result =
            await FirebaseAuth.instance.signInWithCredential(authCredential);
        print('Access token: ${accessToken.token}');

        // Get profile data
        final profile = await fb.getUserProfile();
        print('Hello, ${profile.name}! You ID: ${profile.userId}');

        // Get user profile image url
        final imageUrl = await fb.getProfileImageUrl(width: 100);
        print('Your profile image: $imageUrl');

        // Get email (since we request email permission)
        final email = await fb.getUserEmail();
        // But user can decline permission
        if (email != null) {
          print('And your email is $email');
          // await FirebaseFirestore.instance
          //     .collection('users')
          //     .doc(result.user.uid)
          //     .set({
          //   'username': profile.name,
          //   'email': email.toString(),
          // });

          goToNextScreenAndRemove(context, HomeScreen());
        }

        break;
      case FacebookLoginStatus.cancel:
        // User cancel log in
        break;
      case FacebookLoginStatus.error:
        // Log in failed
        print('Error while log in: ${res.error}');
        break;
    }
  }
}
