import 'dart:async';
import 'package:besthomecare/Screens/homeScreen.dart';
import 'package:besthomecare/utils/LinkingScreens.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'auth_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 2),
        () => goToNextScreenAndRemove(
            context,
            (FirebaseAuth.instance.currentUser == null)
                ? AuthScreen()
                : HomeScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
        color: Colors.white,
        alignment: Alignment.center,
        child: Text('Splash Screen'),
      ),
    ));
  }
}
