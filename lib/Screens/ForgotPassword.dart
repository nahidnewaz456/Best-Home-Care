import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:besthomecare/utils/LinkingScreens.dart';

class ForgotPassword extends StatelessWidget {
  static String id = 'forgot-password';
  var ForgotPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 40, left: 10),
            child: IconButton(
              onPressed: () {
                goBack(context);
              },
              icon: Icon(
                Icons.arrow_back_ios_sharp,
                color: Color(0xFFB2A9EC),
                size: 30,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 150, left: 30),
            child: Text(
              "don't worry",
              style: GoogleFonts.cormorantInfant(
                  fontSize: 36,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFFB2A9EC).withOpacity(0.6)),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 190, left: 30),
            child: Text(
              'we will let you change \nyour password',
              style: GoogleFonts.cormorantInfant(
                  fontSize: 16, color: Color(0xFFA9A9A9)),
            ),
          ),
          Center(
            child: Container(
              margin: EdgeInsets.only(bottom: 10, top: 10, right: 40, left: 40),
              height: 60,
              child: Center(
                child: Theme(
                  data: new ThemeData(
                    primaryColor: Color(0xFFB2A9EC),
                  ),
                  child: TextFormField(
                    controller: ForgotPasswordController,
                    style: GoogleFonts.cormorantInfant(
                        fontWeight: FontWeight.bold),
                    cursorColor: Color(0xFFB2A9EC),
                    key: ValueKey('email'),
                    validator: (value) {
                      if (value.isEmpty ||
                          !value.contains('@') ||
                          !value.contains('.')) {
                        return 'Please enter a valid email address';
                      }
                      return null;
                    },
                    onChanged: (s) {},
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        labelText: 'Email',
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xFFB2A9EC), width: 2.0),
                            borderRadius: BorderRadius.circular(30.0)),
                        hoverColor: Color(0xFFB2A9EC),
                        contentPadding: EdgeInsets.only(left: 20)),
                  ),
                ),
              ),
            ),
          ),
          Center(
            child: GestureDetector(
              onTap: () {
                sendEmail(ForgotPasswordController.text);
                goBack(context);
              },
              child: Container(
                width: 200,
                height: 60.0,
                alignment: Alignment.center,
                margin: EdgeInsets.only(left: 20, right: 20, top: 150),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Color(0xFFB2A9EC)),
                child: Text(
                  'Send Email ',
                  style: GoogleFonts.cormorantInfant(
                      color: Colors.white,
                      fontSize: 28,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  sendEmail(String email) {
    FirebaseAuth.instance.sendPasswordResetEmail(email: email);
  }
}
