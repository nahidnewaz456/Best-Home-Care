import 'package:besthomecare/Screens/auth_screen.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:besthomecare/utils/LinkingScreens.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
      onTap: () {
        FirebaseAuth.instance.signOut().then((value) {
          print('res: done');

          goToNextScreenAndRemove(context, AuthScreen());
        }).onError((error, stackTrace) {
          print('res: $error');
        });
      },
      child: Center(
          child: Container(
              height: 50,
              width: 80,
              child: Card(
                child: Center(child: Text('Sign out')),
              ))),
    ));
  }
}
